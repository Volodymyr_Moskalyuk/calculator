package com.griddynamics.calculator;


import java.util.*;

public class Calculation {
    private final String OPERATORS = "+-*/^%";
    private final String[] FUNCTIONS = {"sin", "cos", "tan", "sqrt", "abs", "pow"};
    private final String SEPARATOR = ",";
    public Stack<String> sStack = new Stack<>();
    public StringBuilder sbOut = null;

    CalculatorOperation calculatorOperation = new CalculatorOperation();

    public String convertString(String inputString) throws Exception {

        inputString.replace("-", "-0");
        sbOut = new StringBuilder("");

        StringTokenizer stringTokenizer = new StringTokenizer(inputString, OPERATORS + SEPARATOR + " ()", true);

        fillStack(stringTokenizer);

        while (!sStack.empty()) {
            sbOut.append(" ").append(sStack.pop());
        }
        return sbOut.toString();
    }

    private int getPriority(String op) {
        Map<String, Integer> operationMap = new HashMap<>() {{
            put("^", 3);
            put("/", 2);
            put("*", 2);
            put("+", 1);
            put("-", 1);
            put("%", 2);
        }};
        return operationMap.get(op);
    }

    private boolean isFunction(String token) {
        return Arrays.stream(FUNCTIONS).anyMatch(token::equals);
    }

    private boolean isOperation(String c) {
        String[] operations = OPERATORS.split("");
        return Arrays.stream(operations).anyMatch(c::equals);
    }

    private void fillStack(StringTokenizer stringTokenizer) throws Exception {
        String temp;
        while (stringTokenizer.hasMoreTokens()) {
            String token = stringTokenizer.nextToken();
            if (isOperation(token)) {

                while (!sStack.empty()) {
                    if (isOperation(sStack.lastElement()) && (getPriority(token) <= getPriority(sStack.lastElement()))) {
                        sbOut.append(" ").append(sStack.pop()).append(" ");
                    } else {
                        sbOut.append(" ");
                        break;
                    }
                }
                sStack.push(token);
            } else if (token.equals("(")) {
                sStack.push(token);
            } else if (token.equals(")")) {
                temp = sStack.lastElement();
                while (!temp.equals("(")) {
                    if (sStack.size() < 1) {
                        throw new Exception("Expression error");
                    }
                    sbOut.append(" ").append(temp).append(" ");
                    sStack.remove(temp);
                    temp = sStack.lastElement();
                }
                sStack.remove(temp);
                if (sStack.size() > 0) {
                    if (isFunction(sStack.lastElement())) {
                        sbOut.append(" ").append(sStack.lastElement()).append(" ");
                        sStack.remove(sStack.lastElement());
                    }
                }
            } else if (isFunction(token)) {
                sStack.push(token);
            } else {
                sbOut.append(" ").append(token).append(" ");
            }
        }
    }

    public double calculate(String inputString) throws Exception {
        double dA = 0, dB = 0;
        String sTmp;
        Deque<Double> stack = new ArrayDeque<>();
        StringTokenizer tokenizer = new StringTokenizer(inputString);
        while (tokenizer.hasMoreTokens()) {
            try {
                sTmp = tokenizer.nextToken().trim();
                if ((1 == sTmp.length() && isOperation(sTmp)) || isFunction(sTmp)) {
                    if (Arrays.stream(FUNCTIONS).anyMatch(sTmp::equals)) {
                        dA = stack.pop();
                        dA = calculatorOperation.trigonometryOperation(sTmp, dA);
                        stack.push(dA);
                    }
                    if (1 == sTmp.length() && isOperation(sTmp)) {
                        dB = stack.pop();
                        dA = stack.pop();
                        dA = calculatorOperation.standartOperationCalc(sTmp, dB, dA);
                        stack.push(dA);
                    }
                } else {
                    dA = Double.parseDouble(sTmp);
                    stack.push(dA);
                }
            } catch (Exception e) {
                throw new Exception("Wrong character in expressions");
            }
        }
        if (stack.size() > 1) {
            throw new Exception("Wrong number of operations");
        }

        return stack.pop();
    }

}
