package com.griddynamics.calculator;

public interface Operation {
    double calculate();
}
