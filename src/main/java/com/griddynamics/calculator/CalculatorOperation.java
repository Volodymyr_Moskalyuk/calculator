package com.griddynamics.calculator;

import java.util.HashMap;
import java.util.Map;

public class CalculatorOperation {
    Map<String, Operation> operationMap = new HashMap<>();

    public double trigonometryOperation(String function, double value) {
        operationMap.put("sin", () -> Math.sin(value));
        operationMap.put("cos", () -> Math.cos(value));
        operationMap.put("tang", () -> Math.tan(value));
        operationMap.put("sqrt", () -> Math.sqrt(value));
        operationMap.put("abs", () -> Math.abs(value));
        return operationMap.get(function).calculate();
    }

    public double standartOperationCalc(String function, double firstValue, double secondValue)
    {
        operationMap.put("+",() -> firstValue+secondValue);
        operationMap.put("-",() -> firstValue-secondValue);
        operationMap.put("/",() -> firstValue/secondValue);
        operationMap.put("*",() -> firstValue*secondValue);
        operationMap.put("%",() -> firstValue%secondValue);
        operationMap.put("^",() -> Math.pow(firstValue, secondValue));
        return operationMap.get(function).calculate();
    }
}
